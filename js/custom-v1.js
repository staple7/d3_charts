$('#supply-dropdown,#year-dropdown,#demand-dropdown,#from-dropdown').change(function(){
    animateCSS('#'+$(this).attr('id'), 'jello');
    clearDivbefore();
    dataFromTextDropDown1();
    
});

$('#supply-dropdown2,#year-dropdown2,#demand-dropdown2,#from-dropdown2').change(function(){
    animateCSS('#'+$(this).attr('id'), 'jello');
    clearDivbefore2();
    dataFromTextDropDown2();
    
});


$('#word-dropdown').change(function(){
    var djg = $(this).val();
    $("#width_tmp_option").html($('#word-dropdown option:selected').text()); 
    $(this).width($("#width_tmp_select").width()); 
    animateCSS('#'+$(this).attr('id'), 'jello');
    clearDivbefore();
    dataFromTextDropDown1();
});

$('#word-dropdown2').change(function(){
    var djg = $(this).val();
    $("#width_tmp_option2").html($('#word-dropdown2 option:selected').text()); 
    $(this).width($("#width_tmp_select2").width()); 
    animateCSS('#'+$(this).attr('id'), 'jello');
    clearDivbefore2();
    dataFromTextDropDown2();
});





$('.showNextLevel').click(function(){
   $('.next-level').show();
    clearDivbefore2();
    updateDatafromAnyRadioTrigger2();
    $(this).hide();
});





function appendRadio(data,id,name){
	var html = '';
  if(name=='columnA'){
  for(var i = 0;i<data.length;i++){
   if(data[i]=='Global'){ 
      html+= '<label class="custom-lable"><input type="radio" name="'+name+'" value="'+data[i]+'" class="my_radio_box" checked>'+data[i]+"</label>"; 
     }else{
      html+= '<label class="custom-lable"><input type="radio" name="'+name+'" value="'+data[i]+'" class="my_radio_box">'+data[i]+"</label>"; 
     }
  }
  }else if(name=='columnE'){
   for(var i = 0;i<data.length;i++){
   if(data[i]=='2024'){ 
      html+= '<label class="custom-lable"><input type="radio" name="'+name+'" value="'+data[i]+'" class="my_radio_box" checked>'+data[i]+"</label>"; 
     }else{
      html+= '<label class="custom-lable"><input type="radio" name="'+name+'" value="'+data[i]+'" class="my_radio_box">'+data[i]+"</label>"; 
     }
  }
  }else{
	for(var i = 0;i<data.length;i++){
	 if(i==0){ 
      html+= '<label class="custom-lable"><input type="radio" name="'+name+'" value="'+data[i]+'" class="my_radio_box" checked>'+data[i]+"</label>"; 
     }else{
     	html+= '<label class="custom-lable"><input type="radio" name="'+name+'" value="'+data[i]+'" class="my_radio_box">'+data[i]+"</label>"; 
     }
	}
}
	$('#'+id).append(html);
}
function appendCheckBox(data,id,name){
    var html = '';
    var check = 'true';
  if(name=='columnF'){
   for(var i = 0;i<data.length;i++){
     if(data[i]=='Coal' || data[i]=='Oil&Gas'){
       if(check=='true'){ 
       html+= '<label class="custom-lable"><input type="checkbox" name="'+name+'" value="Fossil" checked>Fossil Fuel</label>';
       check = 'false';
      }
      }else{
      html+= '<label class="custom-lable"><input type="checkbox" name="'+name+'" value="'+data[i]+'" checked>'+data[i]+"</label>";
      }
  }
  }else{  
	for(var i = 0;i<data.length;i++){
		 html+= '<label class="custom-lable"><input type="checkbox" name="'+name+'" value="'+data[i]+'" checked>'+data[i]+"</label>";
 	}
 }
	$('#'+id).append(html);
}


   var columnA = [];
   var columnB = [];       
   var columnC = [];       
   var columnD = [];       
   var columnE = [];       
   var columnF = [];       
   var columnG = [];       
   var columnH = ["Under-supply","Over-supply"]; 
   var finalArray = [];      
	//d3.csv("ColumnChartTestData.csv", function(dt){
		function getdata(){
		       $('.loader-top').show();
			   for(var i = 0;i <= dt.length;i++ ){
			   	let a = dt[i]["Portfolio.Name"];
		         if(columnA.indexOf(a) === -1){
			       columnA.push(a);
			    }
			     if(columnB.indexOf(dt[i].Scenario) === -1){
			       columnB.push(dt[i].Scenario);
			    }
			    if(columnC.indexOf(dt[i].Allocation) === -1){
			       columnC.push(dt[i].Allocation);
			    }
			    if(columnD.indexOf(dt[i].ScenarioGeography) === -1){
			       columnD.push(dt[i].ScenarioGeography);
			    }
			    if(columnE.indexOf(dt[i].Year) === -1){
			       columnE.push(dt[i].Year);
			    }
			    if(columnF.indexOf(dt[i].Sector) === -1){
			       columnF.push(dt[i].Sector);
			    }
			    if(columnG.indexOf(dt[i].Technology) === -1){
			       columnG.push(dt[i].Technology);
			    }
			    finalArray.push(dt[i]);
			  }
		}
		
setTimeout(function(){
  appendRadio(columnA,'columnA','columnA');
  appendRadio(columnB,'columnB','columnB');
  appendRadio(columnC,'columnC','columnC');
  appendRadio(columnD,'columnD','columnD');
  appendRadio(columnE,'columnE','columnE');
  appendCheckBox(columnF,'columnF','columnF');
  appendCheckBox(columnG,'columnG','columnG');
  appendRadio(columnH,'columnH','columnH');
  $('.dropbox_panel').show();
   $('.loader-top').hide();
  updateDatafromAnyRadioTrigger();
},3000);



function createArrayinFormat(data,a,b,c,d,e){
  var array = [];
  var arrayb = [];
  var arrayc = [];
  var arrayd = [];
  var arraye = [];
 
  for(var i = 0;i < data.length;i++){
    if(data[i]["Portfolio.Name"]==a){
     array.push(data[i]);
    }
  }

  for(var i = 0;i < array.length;i++){
    if(array[i].Scenario==b){
     arrayb.push(array[i]);
    }
  }

  for(var i = 0;i < arrayb.length;i++){
    if(arrayb[i].Allocation==c){
     arrayc.push(arrayb[i]);
    }
  }


  for(var i = 0;i < arrayc.length;i++){
    if(arrayc[i].ScenarioGeography==d){
     arrayd.push(arrayc[i]);
    }
  }

  for(var i = 0;i < arrayd.length;i++){
    if(arrayd[i].Year==e){
     arraye.push(arrayd[i]);
    }
  }
  return arraye;
}


function handleSectorAndTechnology(data){
	var arrayesds = [];
	var colF = []; 
	var colG = [];
	$.each($("input[name='columnF']:checked"), function(){
       if($(this).val()=='Fossil'){
        colF.push("Coal");
        colF.push("Oil&Gas");
       }else{
           colF.push($(this).val());
       }
    });
   $.each($("input[name='columnG']:checked"), function(){
        colG.push($(this).val());
    });
  
 

   for(var i = 0;i < data.length;i++){
   	if (typeof colF !== 'undefined' && colF.length > 0) {
   	if(colF.indexOf(data[i].Sector) !== -1){
      if(colG.indexOf(data[i].Technology)!==-1){
      	arrayesds.push(data[i]);
      }
   	}
   }else{
   	 return arrayesds; 
   }
  }
  return arrayesds;
}



function appendDiv(section,tag,icon,iswarinig,rate,year){
	var warn = '';
 if(iswarinig=='true'){
    warn = '<div class="warning-icon" data-toggle="modal" data-target="#sideModalTR"><img src="image/warning-icon.png"></div>';	 
  }else{
   if(icon=='Oil' || icon=='Gas'){	
      if(year > 2022){
	   warn = '<div class="warning-icon2" data-toggle="modal" data-target="#sideModalTR2" ><img src="image/warning2-icon.png"></div>';
	 }
	}else{
	warn = '';	
	} 
  }

 var imageColor = pickColorForRange(rate);
 var yoyo = imageColor.replace("#", "");
 var imageUrl = icon+'-'+yoyo+'.svg';
 let iconUp = '';
 if(rate > 100){
   iconUp = '<div class="arraw-icon"><img src="image/arrow-chevron.png"></div>';
 }


 let secondWarningIcon = '';
 if(icon=='Oil' || icon=='Gas'){
   if(year > 2022){
   	secondWarningIcon = ''
   }
 }


  var html = '<div class="part-div"><span class="top-value">100%</span><div class="dish-box">'+iconUp+'<div class="d-flex align-items-start flex-column space-around graph-custom-div" id="legend-'+section+'-'+tag+'"></div><div id="chart-'+section+'-'+tag+'"></div><div class="widget-header" id="frame-'+section+'-'+tag+'"></div><div class="logo-bottom"><img src="image/'+imageUrl+'">'+warn+'</div></div><span class="last-value">0%</span></div>';
   $('.'+section).append(html);
}


function getElement4Text(rangde,cat,cf){
	var unit= '';
	var txt = '';
	let range = rangde;
	if(cf=='Over-supply'){
		if(range < 0){
			range = range * -1;
		}
	}

   let finalRange = range;

	if(cat=='Oil' || cat=='Gas' || cat=='Coal' || cat=='ICE' || cat=='Electric' || cat=='Hybrid'){
     if(range > 1000000000){
     	finalRange = range/1000000000;
        unit = 'billion'; 
     }if(range > 1000000){
     	unit = 'million';
     	finalRange = range/1000000;
     }if(range > 1000){
     	unit = 'thousand';
     	finalRange = range/1000;
     }if(range < 1000){
     	unit = '';
     	//finalRange = range;
     }
	}else{
		if(range > 1000000000){
		unit = 'TW'; 
		finalRange = range/1000000;
		}if(range > 1000000){
		unit = 'TW';
		finalRange = range/1000000;
		}if(range > 1000){
		unit = 'GW';
		finalRange = range/1000;
		}if(range < 1000){
		 unit = 'MW';
		}
	}


	if(cat=='Oil'){
		if(cf=='Over-supply'){
			txt = 'barrels/year in over-supply';
		}else{
			txt = 'barrels/year in under-supplied';
		}
	}

	if(cat=='Gas'){
		if(cf=='Over-supply'){
			txt = 'cubic meters/year in over-supply';
		}else{
			txt = 'cubic meters/year under-supplied';
		}
	}

	if(cat=='Coal'){
		if(cf=='Over-supply'){
			txt = 'tonnes/year in over-supply';
		}else{
			txt = 'tonnes/year in under-supplied';
		}
	}

	if(cat=='ICE' || cat=='Electric' || cat=='Hybrid'){
		if(cf=='Over-supply'){
			txt = 'veh./year left  in over-supply';
		}else{
			txt = 'veh./year left to produce';
		}
	}

	if(cat=='RenewablesCap' || cat=='HydroCap' || cat=='NuclearCap' || cat=='OilCap' || cat=='GasCap' || cat=='CoalCap'){
		if(cf=='Over-supply'){
			txt = 'left to retire';
		}else{
			txt = 'left to build';
		}
	}


    let getRoundRange = rangeCalculator(~~finalRange); 
	  let trddfdf=getRoundRange+' '+unit+' '+txt;
	return trddfdf;



}

function rangeCalculator(range){
	//console.log(range);
  let finalRange;
	if(range < 10){
	  finalRange = Math.round(range/5)*5;
    }if(range <= 100 && range > 10){
	   finalRange = Math.round(range/10)*10; 	
	  }if(range <= 1000 && range > 100){
		finalRange = Math.round(range/100)*100; 	
	 }if(range <= 10000 && range > 1000){
		finalRange = Math.round(range/100)*100; 	
	}if(range <= 100000 && range > 10000){
		finalRange = Math.round(range/1000)*1000; 	
	}if(range <= 1000000 && range > 100000){
		finalRange = Math.round(range/10000)*10000; 	
	}if(range <= 10000000 && range > 1000000){
		finalRange = Math.round(range/10000)*10000; 	
	}if(range <= 100000000 && range > 10000000){
		finalRange = Math.round(range/100000)*100000; 	
	}if(range > 100000000){
		finalRange = Math.round(range/100000)*100000; 	
	}
  return finalRange;
}


function printD3chart2(p,f,a,cf){
$('.first_block2').hide(); 
$('.second_block2').hide();  
$('.third_block2').hide(); 
  var power = []; 
  var fossile = []; 
  var automation = []; 
  if(p.length > 0){ 
    $('.first_block2').show();
   for(var i = 0; i < p.length;i++){
      var dadfd = [];
      var temp = {};
      let sds = ((p[i].PlannedProduction - p[i].RequiredProduction)/p[i].RequiredProduction);
      let rate = sds.toFixed(2);

      if(rate < 0){
       rate = (rate*-1);
      }
       rate = rate*100;
      let point = rate;

      let warning = 'false';
      let cat2 = (100-rate);
      if(rate > 100){
        warning = 'true';
        cat2 = 0;
      }
      let txt = (p[i].PlannedProduction - p[i].RequiredProduction);
      let years = p[i].Year;
      temp = {
         "Year": "201"+(i+1),
         "Category1": rate,
         "Category2": cat2.toFixed(2),
      }
      dadfd.push(temp);
      let imageName = p[i].Technology;
      txt = getElement4Text(txt,imageName,cf);

      appendDiv('power2',i+1,imageName,warning,rate,years);
      drawStackChart(dadfd,'power2-'+(i+1),txt,point);
   }
  }
  if(f.length > 0){ 
    $('.second_block2').show();
    $('.power2').addClass('firstb');
   for(var i = 0; i < f.length;i++){
     var dadfd = [];
      var temp = {};
      let sds = ((f[i].PlannedProduction - f[i].RequiredProduction)/f[i].RequiredProduction);

      let rate = sds.toFixed(2);
      if(rate < 0){
        rate = (rate*-1);
      }
      rate = rate * 100;
      let point = rate;

      let warning = 'false';
      let cat2 = (100-rate);
      if(rate > 100){
        warning = 'true';
        cat2 = 0;
      }
      let txt = (f[i].PlannedProduction - f[i].RequiredProduction);
      let years = f[i].Year;
      temp = {
         "Year": "201"+(i+1),
         "Category1": rate,
         "Category2": cat2.toFixed(2),
      }
       let imageName = f[i].Technology;
       txt = getElement4Text(txt,imageName,cf);
      dadfd.push(temp);
      appendDiv('fossile2',i+1,imageName,warning,rate,years);
      drawStackChart(dadfd,'fossile2-'+(i+1),txt,point);
   }
  }

  if(a.length > 0){
  $('.third_block2').show(); 
  $('.power2').addClass('firstb');
  $('.fossile2').addClass('firstb');
  $('.automation2').addClass('bajaj');
   for(var i = 0; i < a.length;i++){
      var temp = {};
      var dadfd = [];
      let sds = ((a[i].PlannedProduction - a[i].RequiredProduction)/a[i].RequiredProduction);
     // console.log(sds);
      let rate = sds.toFixed(2);
      if(rate < 0){
        rate = (rate*-1);
      }
      rate = rate * 100;
      let point = rate;

      let warning = 'false';
      let cat2 = (100-rate);
      if(rate > 100){
        warning = 'true';
        cat2 = 0;
      }
      let txt = (a[i].PlannedProduction - a[i].RequiredProduction);
      let years = a[i].Year;

      temp = {
         "Year": "201"+(i+1),
         "Category1": rate,
         "Category2": cat2.toFixed(2),
      }
      let imageName = a[i].Technology;
      txt = getElement4Text(txt,imageName,cf);
      dadfd.push(temp);
      appendDiv('automation2',i+1,imageName,warning,rate,years);
      drawStackChart(dadfd,'automation2-'+(i+1),txt,point);
   }
  }
}



function printD3chart(p,f,a,cf){
$('.first_block').hide();	
$('.second_block').hide();	
$('.third_block').hide();	
  var power = []; 
  var fossile = []; 
  var automation = []; 
  if(p.length > 0){ 
  	$('.first_block').show();
   for(var i = 0; i < p.length;i++){
   	  var dadfd = [];
      var temp = {};
      let sds = ((p[i].PlannedProduction - p[i].RequiredProduction)/p[i].RequiredProduction);
      let rate = sds.toFixed(2);

      if(rate < 0){
       rate = (rate*-1);
      }
       rate = rate*100;
      let point = rate;

      let warning = 'false';
      let cat2 = (100-rate);
      if(rate > 100){
      	warning = 'true';
      	cat2 = 0;
      }
      //console.log(rate);
      let txt = (p[i].PlannedProduction - p[i].RequiredProduction);
      let years = p[i].Year;

      temp = {
      	 "Year": "201"+(i+1),
      	 "Category1": rate,
      	 "Category2": cat2.toFixed(2),
      }
      dadfd.push(temp);
      let imageName = p[i].Technology;
      txt = getElement4Text(txt,imageName,cf);
      //console.log(txt);

      appendDiv('power',i+1,imageName,warning,rate,years);
      drawStackChart(dadfd,'power-'+(i+1),txt,point);
   }
  }
  if(f.length > 0){ 
  	$('.second_block').show();
  	$('.power').addClass('firstb');
   for(var i = 0; i < f.length;i++){
   	 var dadfd = [];
      var temp = {};
      let sds = ((f[i].PlannedProduction - f[i].RequiredProduction)/f[i].RequiredProduction);

      let rate = sds.toFixed(2);
      if(rate < 0){
        rate = (rate*-1);
      }
      rate = rate * 100;
      let point = rate;

      let warning = 'false';
      let cat2 = (100-rate);
      if(rate > 100){
      	warning = 'true';
      	cat2 = 0;
      }
      let txt = (f[i].PlannedProduction - f[i].RequiredProduction);
      let years = f[i].Year;
      temp = {
      	 "Year": "201"+(i+1),
      	 "Category1": rate,
      	 "Category2": cat2.toFixed(2),
      }
       let imageName = f[i].Technology;
       txt = getElement4Text(txt,imageName,cf);
      dadfd.push(temp);
      appendDiv('fossile',i+1,imageName,warning,rate,years);
      drawStackChart(dadfd,'fossile-'+(i+1),txt,point);
   }
  }

  if(a.length > 0){
  $('.third_block').show(); 
  $('.power').addClass('firstb');
  $('.fossile').addClass('firstb');
  $('.automation').addClass('bajaj');
   for(var i = 0; i < a.length;i++){
      var temp = {};
      var dadfd = [];
      let sds = ((a[i].PlannedProduction - a[i].RequiredProduction)/a[i].RequiredProduction);
      let rate = sds.toFixed(2);
      if(rate < 0){
      	rate = (rate*-1);
      }
      rate = rate * 100;
      let point = rate;

      let warning = 'false';
      let cat2 = (100-rate);
      if(rate > 100){
      	warning = 'true';
      	cat2 = 0;
      }
      let txt = (a[i].PlannedProduction - a[i].RequiredProduction);
      let years = a[i].Year;

      temp = {
      	 "Year": "201"+(i+1),
      	 "Category1": rate,
      	 "Category2": cat2.toFixed(2),
      	 //"Technology": a[i].Technology
      }
      let imageName = a[i].Technology;
      //console.log(txt);
      txt = getElement4Text(txt,imageName,cf);
      dadfd.push(temp);
      appendDiv('automation',i+1,imageName,warning,rate,years);
      drawStackChart(dadfd,'automation-'+(i+1),txt,point);
   }
  }
}



function categoriseChart2(data,cf){
  var power = [];
  var fussl = [];
  var automation = [];
  for(var i = 0;i< data.length;i++){
    if(data[i].Sector=='Power'){
      power.push(data[i]);
    }
    if(data[i].Sector=='Automotive'){
      automation.push(data[i]);
    }
    if(data[i].Sector=='Coal' || data[i].Sector=='Oil&Gas' ){
      fussl.push(data[i]);
    }
  }
  printD3chart2(power,fussl,automation,cf);  
}

function categoriseChart(data,cf){
  var power = [];
  var fussl = [];
  var automation = [];
  for(var i = 0;i< data.length;i++){
    if(data[i].Sector=='Power'){
    	power.push(data[i]);
    }
    if(data[i].Sector=='Automotive'){
    	automation.push(data[i]);
    }
    if(data[i].Sector=='Coal' || data[i].Sector=='Oil&Gas' ){
    	fussl.push(data[i]);
    }
  }
  printD3chart(power,fussl,automation,cf);  
}

function hidedivByclass(id){
  $('.'+id).hide();

}

function dataFromTextDropDown2(){
   //console.log('dfd');
    let ca = $( "#from-dropdown2 option:selected" ).val();
    let cb = $( "#demand-dropdown2 option:selected" ).val();
    let cc = $( "input[name='columnC']:checked").val();
    let cd = $( "#word-dropdown2 option:selected" ).val();
    let ce = $( "#year-dropdown2 option:selected" ).val();
    let cf = $( "#supply-dropdown2 option:selected" ).val();
    //console.log(cf)
    var hdn = createArrayinFormat(finalArray,ca,cb,cc,cd,ce);
    var temp = handleSectorAndTechnology(hdn);
    var arraytochart = [];

    for(var i=0; i< temp.length;i++){
     if(cf=='Under-supply'){
      if(temp[i].RequiredProduction < temp[i].PlannedProduction){
          arraytochart.push(temp[i]);   
      }
     }else{
      if(temp[i].RequiredProduction > temp[i].PlannedProduction){
        arraytochart.push(temp[i]);
      }
     }
    }
    
    categoriseChart2(arraytochart,cf);
}

function dataFromTextDropDown1(){
   //console.log('dfd');
    let ca = $( "#from-dropdown option:selected" ).val();
    let cb = $( "#demand-dropdown option:selected" ).val();
    let cc = $( "input[name='columnC']:checked").val();
    let cd = $( "#word-dropdown option:selected" ).val();
    let ce = $( "#year-dropdown option:selected" ).val();
    let cf = $( "#supply-dropdown option:selected" ).val();
    //console.log(cf)
    var hdn = createArrayinFormat(finalArray,ca,cb,cc,cd,ce);
    var temp = handleSectorAndTechnology(hdn);
    var arraytochart = [];

    for(var i=0; i< temp.length;i++){
     if(cf=='Under-supply'){
      if(temp[i].RequiredProduction < temp[i].PlannedProduction){
          arraytochart.push(temp[i]);   
      }
     }else{
      if(temp[i].RequiredProduction > temp[i].PlannedProduction){
        arraytochart.push(temp[i]);
      }
     }
    }
    
    categoriseChart(arraytochart,cf);
}




function updateDatafromAnyRadioTrigger(){
	 let ca = $( "input[name='columnA']:checked" ).val();
    let cb = $( "input[name='columnB']:checked" ).val();
    let cc = $( "input[name='columnC']:checked").val();
    let cd = $( "input[name='columnD']:checked" ).val();
    let ce = $( "input[name='columnE']:checked" ).val();
    let cf = $( "input[name='columnH']:checked" ).val();
    var hdn = createArrayinFormat(finalArray,ca,cb,cc,cd,ce);
    var temp = handleSectorAndTechnology(hdn);
    var arraytochart = [];

    for(var i=0; i< temp.length;i++){
     if(cf=='Under-supply'){
     	if(temp[i].RequiredProduction < temp[i].PlannedProduction){
          arraytochart.push(temp[i]);		
     	}
     }else{
     	if(temp[i].RequiredProduction > temp[i].PlannedProduction){
     	  arraytochart.push(temp[i]);
     	}
     }
    }
    categoriseChart(arraytochart,cf);
    updatedropDownfromcheckbox();
}

function updateDatafromAnyRadioTrigger2(){
  let ca = $( "input[name='columnA']:checked" ).val();
    let cb = $( "input[name='columnB']:checked" ).val();
    let cc = $( "input[name='columnC']:checked").val();
    let cd = $( "input[name='columnD']:checked" ).val();
    let ce = $( "input[name='columnE']:checked" ).val();
    let cf = $( "input[name='columnH']:checked" ).val();
    var hdn = createArrayinFormat(finalArray,ca,cb,cc,cd,ce);
    var temp = handleSectorAndTechnology(hdn);
    var arraytochart = [];
    //console.log(cf);

    for(var i=0; i< temp.length;i++){
     if(cf=='Under-supply'){
     	if(temp[i].RequiredProduction < temp[i].PlannedProduction){
          arraytochart.push(temp[i]);		
     	}
     }else{
     	if(temp[i].RequiredProduction > temp[i].PlannedProduction){
     	  arraytochart.push(temp[i]);
     	}
     }
    }
    categoriseChart2(arraytochart,cf);
    updatedropDownfromcheckbox();
}

function clearDivbefore(){
  $('.power').html('');
  $('.fossile').html('');
  $('.automation').html('');
}
function clearDivbefore2(){
	$('.power2').html('');
	$('.fossile2').html('');
	$('.automation2').html('');
}


$('body').on('change', 'input[type="radio"]', function() {
	  clearDivbefore();
	  updateDatafromAnyRadioTrigger();
	  clearDivbefore2();
    updateDatafromAnyRadioTrigger2();
});



$('body').on('change', 'input[type="checkbox"]', function() {
   clearDivbefore();	
   updateDatafromAnyRadioTrigger();
   clearDivbefore2();
    updateDatafromAnyRadioTrigger2();
   //console.log(colG);
});

function updatedropDownfromcheckbox(){
    
    let ca = $( "input[name='columnA']:checked" ).val();
    let cb = $( "input[name='columnB']:checked" ).val();
    let cd = $( "input[name='columnD']:checked" ).val();
    let ce = $( "input[name='columnE']:checked" ).val();
    let cf = $( "input[name='columnH']:checked" ).val();
    $("#supply-dropdown").val(cf).attr('selected', true);
    $("#supply-dropdown2").val(cf).attr('selected', true);
    $("#year-dropdown").val(ce).attr('selected', true);
    $("#year-dropdown2").val(ce).attr('selected', true);
    $("#demand-dropdown").val(cb).attr('selected', true);
    $("#demand-dropdown2").val(cb).attr('selected', true);
    if(cd=='GlobalAggregate'){
      cd = 'Global';
    }

    $("#word-dropdown").val(cd).attr('selected', true);
    $("#word-dropdown2").val(cd).attr('selected', true);
    if(cd =='OECD' || cd =='NonOECD' ){
      $("#word-dropdown").css('width','239px');
      $("#word-dropdown2").css('width','239px');
     }else{
      $("#word-dropdown").css('width','103px');
      $("#word-dropdown2").css('width','103px');
    }

    $("#from-dropdown").val(ca).attr('selected', true);
    $("#from-dropdown2").val(ca).attr('selected', true);
}

$('body').on('click', '.warning-icon', function() {
 //$('#sideModalTR').show();
});

$('body').on('click', '.close', function() {
 $('#myModal').hide();
});


function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}




    //Draw Stack Chart
    var marginStackChart = { top: 10, right: 0, bottom: 0, left: 0 },
            widthStackChart = 500 - marginStackChart.left - marginStackChart.right,
            heightStackChart = 300 - marginStackChart.top - marginStackChart.bottom;

    var xStackChart = d3.scaleBand()
            .range([0, widthStackChart])
            .padding(0.1);
    var yStackChart = d3.scaleLinear()
                .range([heightStackChart, 0]);

         var data = [
             {
                 "Year": "2012",
                 "Category1": "75",
                 "Category2": "425"
             }
        ];
        

       $(document).ready(function () {
           //drawStackChart(data);
           //animateCSS('.chevron', 'bounce')

           getdata();
      });  

        function pickColorForRange(ra){
          	  if(ra > 0 && ra <= 10)
          	   return '#41D582';	

          	  if(ra > 10 && ra <= 30)
          	   return '#7de7b8';
              
              if(ra > 30 && ra <= 50)
          	   return '#f8d234';

          	if(ra > 50)
          	   return '#f76261';
          	else
          		return '#fff';

          	
          }

    function drawStackChart(data,tag,txt,point) {
	var podnnd = point < 0 ? Math.floor(Math.abs(point) * 100) * -1 / 100 : point.toFixed(2).replace(".00", "");
      
     
    	var colorStackChart = d3.scaleOrdinal(["#41D582", "#ddd", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"])

    	var canvasStackChart = d3.select("#chart-"+tag).append("svg")
    	.attr("width",100)
        /*.attr("width", widthStackChart + marginStackChart.left + marginStackChart.right)*/
        .attr("height", heightStackChart + marginStackChart.top + marginStackChart.bottom)
        .append("g")
        .attr("transform", "translate(" + marginStackChart.left + "," + marginStackChart.top + ")");


			colorStackChart.domain(d3.keys(data[0]).filter(function (key) { return key !== "Year"; }));

			data.forEach(function (d) {
			    var y0 = 0;
			    d.ages = colorStackChart.domain().map(function (name) { return { name: name, y0: y0, y1: y0 += +d[name] }; });
			    d.total = d.ages[d.ages.length - 1].y1;
			});

			data.sort(function (a, b) { return b.total - a.total; });

			xStackChart.domain(data.map(function (d) { return d.Year; }));
			yStackChart.domain([0, d3.max(data, function (d) { return d.total; })]);

			var divLegend = d3.select("#legend-"+tag);
				divLegend.append("span")
				    .data(data)
				    .html(podnnd+'%')
				    .attr('class','average')
				    .style('color',function(d){
              if(d.Category1 > 99 ){
                return '#fff';
              }else{
				    	return pickColorForRange(d.Category1);  
              }
				    })
				    .style("bottom",function(d) {
                         let tdsb = +(d.Category1);
                         //console.log(tdsb);
                         if(tdsb > 99){
                         	tdsb = 99;
                         }else{
                             tdsb = tdsb+6;
                         }
				    	 return tdsb+'%'; });

                   divLegend.append('div')
                          .attr('class','mb-auto')
                          .style('width','6px')
                          .style('background-color','#F76262')
                          .style('height','136px')
                          .style('border-radius','30px')
                          .style('margin-bottom','6px');

                   divLegend.append('div')
                          .style('width','6px')
                          .style('background-color','#FBD233')
                          .style('height','65px')
                          .style('border-radius','30px')
                          .style('margin-bottom','6px');

                   divLegend.append('div')
                          .style('width','6px')
                          .style('background-color','#7DE7B8')
                          .style('height','40px')
                          .style('border-radius','30px')
                          .style('margin-bottom','6px');  

                   divLegend.append('div')
                          .style('width','6px')
                          .style('background-color','#41D582')
                          .style('height','24px')
                          .style('border-radius','30px')

                  var frametxt = d3.select('#frame-'+tag);
                          frametxt.append('span')
                                  .data(data)
                                  .html(txt)
                                  .style('color',function(d){
							    	return pickColorForRange(d.Category1);
							    });      
                 
                 var tooltip = d3.select("body").append("div")
                       .data(data)
                       .attr("class", "toolTip")
                       .style('color',function(d){
                         return pickColorForRange(d.Category1);
                       });


             var checkColorTriggered = 'false';
            var state = canvasStackChart.selectAll(".Year")
            .data(data)
            .enter().append("g")
            .attr("class", "g")
            .attr("transform", function (d) { return "translate(" + xStackChart(d.Year) + ",0)"; });

            state.selectAll("rect")
            .data(function (d) { 
            	return d.ages; })
            .enter().append("rect")
            .attr("width", 70)
            .style('opacity',1)

            .attr("y", function (d) { return yStackChart(d.y0); })
            .style("fill", function (d) {
                if(checkColorTriggered=='false'){
                 checkColorTriggered = 'true';
                 return pickColorForRange(d.y1-d.y0);
               }
                else{
                 return pickColorForRange(0);
                }
               })
			.on("mouseover", function(d){
           tooltip
              .style("left", d3.event.pageX - 50 + "px")
              .style("top", d3.event.pageY - 70 + "px")
              .style("display", "inline-block")
              .html(txt);
                //frametxt.selectAll('span').style('display','block');
	              d3.select(this).attr("r", 10).style("opacity", 0.8);
	        })
			.on('mouseout', function(d){
				//let dfgvf = pickColorForRange(d.y1-d.y0)
        //frametxt.style('opacity',0.8);
				 //frametxt.selectAll('span').style('display','none');
         tooltip.style("display", "none");

				d3.select(this).attr("r", 10).style("opacity", 1);
			})
              .attr("height",0)
              .transition()
              .duration(750) 
              .delay(1000)
              .attr("y",  d => { return yStackChart(d.y1); })
              .attr("height",  d => { 
                 //console.log(yStackChart(d.y0) - yStackChart(d.y1));
              	return yStackChart(d.y0) - yStackChart(d.y1); });

    }